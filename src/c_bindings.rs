#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

mod error_handling;

#[allow(dead_code)]
#[repr(C)]
pub enum tc_LogLevel {
    OFF,
    ERROR,
    WARN,
    INFO,
    DEBUG,
    TRACE,
}

#[no_mangle]
pub extern "C" fn tc_initLogging(level: tc_LogLevel) {
    env_logger::Builder::new()
        .filter_level(match level {
            tc_LogLevel::OFF => log::LevelFilter::Off,
            tc_LogLevel::ERROR => log::LevelFilter::Error,
            tc_LogLevel::WARN => log::LevelFilter::Warn,
            tc_LogLevel::INFO => log::LevelFilter::Info,
            tc_LogLevel::DEBUG => log::LevelFilter::Debug,
            tc_LogLevel::TRACE => log::LevelFilter::Trace,
        })
        .init();
}

/// A tc_MessageDBEntry array: 'messages' points to the first element, 'size' is the number of elements.
#[repr(C)]
pub struct tc_MessageDBEntryArray {
    messages: *mut tc_MessageDBEntry,
    size: u32,
}

impl Drop for tc_MessageDBEntryArray {
    fn drop(&mut self) {
        unsafe {
            drop(Vec::from_raw_parts(
                self.messages,
                self.size as usize,
                self.size as usize, // We can be sure that the capacity equals the length because we called into_boxed_slice() before we created the pointer.
            ));
        }
    }
}

/// A message read from the database.
#[repr(C)]
pub struct tc_MessageDBEntry {
    pub chatId: u32,
    pub participantId: u32,
    pub messageId: u32,
    pub content: *mut std::ffi::c_char,
    pub timestamp: *mut std::ffi::c_char,
}

// We have to recreate and drop the CString or else we would only drop the pointer and leak memory.
impl Drop for tc_MessageDBEntry {
    fn drop(&mut self) {
        unsafe {
            drop(std::ffi::CString::from_raw(self.content));
            drop(std::ffi::CString::from_raw(self.timestamp));
        }
    }
}

/// Loads a range of messages of a chat from the database.
/// Returns a tc_MessageDBEntryArray that has to be destroyed using tc_destroyMessageDBEntryArray to avoid memory leaks.
#[no_mangle]
pub extern "C" fn tc_loadMessages(chatId: u32, from: u32, to: u32) -> *mut tc_MessageDBEntryArray {
    message_db_entries_to_ffi_array(crate::database::load_messages(chatId, (from, to)))
        .unwrap_or_else(|err| error_handling::error_null(err))
}

/// Loads the latest messages of a chat from the database.
/// Returns a tc_MessageDBEntryArray that has to be destroyed using tc_destroyMessageDBEntryArray to avoid memory leaks.
#[no_mangle]
pub extern "C" fn tc_loadLatestMessages(chatId: u32, amount: u32) -> *mut tc_MessageDBEntryArray {
    message_db_entries_to_ffi_array(crate::database::load_latest_messages(chatId, amount))
        .unwrap_or_else(|err| error_handling::error_null(err))
}

/// Transforms the message entries into an ffi compatible type.
fn message_db_entries_to_ffi_array(
    entries: Result<Vec<crate::database::MessageDBEntry>, Box<dyn std::error::Error>>,
) -> Result<*mut tc_MessageDBEntryArray, Box<dyn std::error::Error>> {
    let entries = entries?;

    let mut array_vec: Vec<tc_MessageDBEntry> = Vec::new();
    for entry in entries {
        array_vec.push(tc_MessageDBEntry {
            chatId: entry.chat_id,
            participantId: entry.participant_id,
            messageId: entry.message_id,
            content: std::ffi::CString::new(entry.content)?.into_raw(),
            timestamp: std::ffi::CString::new(entry.timestamp)?.into_raw(),
        });
    }

    let array_len = array_vec.len();
    let mut array_box = array_vec.into_boxed_slice(); // Drop extra capacity, so we don't have to track it.
    let array_ptr = array_box.as_mut_ptr();
    std::mem::forget(array_box); // Prevent array_box and the contained messages from being dropped.

    Ok(Box::into_raw(Box::new(tc_MessageDBEntryArray {
        messages: array_ptr,
        size: array_len as u32,
    })))
}

/// Destroys a tc_MessageDBEntry to free the memory.
#[no_mangle]
pub unsafe extern "C" fn tc_destroyMessageDBEntryArray(array: *mut tc_MessageDBEntryArray) {
    drop(Box::from_raw(array));
}
