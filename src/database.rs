use rusqlite::Connection;
use std::sync::Mutex;

lazy_static::lazy_static! {
    static ref DB_CONNECTION: Mutex<Connection> = Mutex::new(open_database_connection());
}

/// A message read from the database.
pub struct MessageDBEntry {
    pub chat_id: u32,
    pub participant_id: u32,
    pub message_id: u32,
    pub content: String,
    pub timestamp: String,
}

/// Loads a range of messages in a chat from the database.
pub fn load_messages(
    chat_id: u32,
    range: (u32, u32),
) -> Result<Vec<MessageDBEntry>, Box<dyn std::error::Error>> {
    let conn = DB_CONNECTION.lock()?;

    let mut stmt = conn.prepare(
        "
            SELECT *
            FROM message
            WHERE chat_id = ?1 AND message_id >= ?2 AND message_id <= ?3
            ORDER BY message_id ASC
        ;",
    )?;
    let mut rows = stmt.query([chat_id, range.0, range.1])?;

    let mut messages: Vec<MessageDBEntry> = Vec::new();
    while let Some(row) = rows.next()? {
        messages.push(MessageDBEntry {
            chat_id: row.get(0)?,
            participant_id: row.get(1)?,
            message_id: row.get(2)?,
            content: row.get(3)?,
            timestamp: row.get(4)?,
        })
    }

    Ok(messages)
}

/// Loads the latest messages in a chat from the database.
pub fn load_latest_messages(
    chat_id: u32,
    amount: u32,
) -> Result<Vec<MessageDBEntry>, Box<dyn std::error::Error>> {
    let conn = DB_CONNECTION.lock()?;

    let mut stmt = conn.prepare(
        "
            SELECT *
            FROM message
            WHERE chat_id = ?1
            ORDER BY message_id DESC
            LIMIT ?2
        ;",
    )?;
    let mut rows = stmt.query([chat_id, amount])?;

    let mut messages: Vec<MessageDBEntry> = Vec::new();
    while let Some(row) = rows.next()? {
        messages.insert(
            0,
            MessageDBEntry {
                chat_id: row.get(0)?,
                participant_id: row.get(1)?,
                message_id: row.get(2)?,
                content: row.get(3)?,
                timestamp: row.get(4)?,
            },
        )
    }

    Ok(messages)
}

fn open_database_connection() -> Connection {
    let project_dirs = directories::ProjectDirs::from("org", "turbochat", "turbochat").unwrap();
    let user_data_dir = project_dirs.data_dir();

    std::fs::create_dir_all(user_data_dir).unwrap();
    let conn = Connection::open(user_data_dir.join("turbochat.db3")).unwrap();

    conn.execute(
        "CREATE TABLE IF NOT EXISTS chat (
            chat_id         INT NOT NULL,
            user_id         INT NOT NULL
        );",
        [],
    )
    .unwrap();
    conn.execute(
        "CREATE TABLE IF NOT EXISTS message (
            chat_id         INT NOT NULL,
            participant_id  INT NOT NULL,
            message_id      INT NOT NULL,
            content         TEXT,
            timestamp       TIMESTAMP
        );",
        [],
    )
    .unwrap();

    conn
}
