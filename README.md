The client library for the decentralised, encrypted Discord-like chat platform Turbochat.

For the official GUI (QT) see: https://gitlab.com/turbochat/turbochat-client-gui

The library exposes its functions using the C ABI, so it can be linked to from more languages.
