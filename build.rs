use cbindgen;
use std::env;
use std::path::PathBuf;

fn main() {
    let crate_dir = env::var("CARGO_MANIFEST_DIR").unwrap();
    let package_name = env::var("CARGO_PKG_NAME").unwrap();
    let output_file = PathBuf::from("include").join(format!("{}.h", package_name));

    cbindgen::generate_with_config(
        &crate_dir,
        cbindgen::Config {
            language: cbindgen::Language::C,
            cpp_compat: true,
            ..Default::default()
        },
    )
    .unwrap()
    .write_to_file(&output_file);
}
